import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.border.EtchedBorder;

public class Main {

	String apeidoP, apeidoM, nambre;
	float fondos;
	
	private JFrame frame;
	private JTextField textCuenta;
	private JTextField textNip;
	private JTextField textAPP;
	private JTextField textNombre;
	private JTextField textAPM;
	private JTextField textNipCre;
	private JTextField textNoCuenta;
	private JTextField textidCliente;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 760, 454);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//CREACION DE OBJETOS
		
		Conexion c = new Conexion();
		Usuario p = new Usuario();
		
		
		
		JPanel ventana1 = new JPanel();
		ventana1.setForeground(Color.RED);
		ventana1.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		ventana1.setBackground(Color.LIGHT_GRAY);
		ventana1.setBounds(10, 5, 724, 399);
		ventana1.setVisible(true);
		frame.getContentPane().add(ventana1);
		
		
		
		
		JPanel IniciarSesion = new JPanel();
		IniciarSesion.setBounds(64, 130, 254, 258);
		IniciarSesion.setForeground(new Color(220, 20, 60));
		IniciarSesion.setVisible(false);
		ventana1.setLayout(null);
		ventana1.add(IniciarSesion);
		IniciarSesion.setLayout(null);
		
		JPanel CrearCuenta = new JPanel();
		CrearCuenta.setBounds(410, 130, 254, 258);
		CrearCuenta.setVisible(false);
		ventana1.add(CrearCuenta);
		CrearCuenta.setLayout(null);
		
		
		textAPP = new JTextField();
		textAPP.setBounds(116, 76, 115, 20);
		CrearCuenta.add(textAPP);
		textAPP.setColumns(10);
		
		JLabel error1 = new JLabel("Debe llenar todos los campos");
		error1.setForeground(Color.RED);
		error1.setFont(new Font("DialogInput", Font.BOLD | Font.ITALIC, 12));
		error1.setBounds(33, 200, 211, 16);
		error1.setVisible(false);
		CrearCuenta.add(error1);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 48, 78, 17);
		CrearCuenta.add(lblNombre);
		
		JLabel lblApellidoPaterno = new JLabel("Apellido paterno");
		lblApellidoPaterno.setBounds(10, 76, 106, 20);
		CrearCuenta.add(lblApellidoPaterno);
		
		textNombre = new JTextField();
		textNombre.setBounds(116, 45, 115, 20);
		CrearCuenta.add(textNombre);
		textNombre.setColumns(10);
		
		textAPM = new JTextField();
		textAPM.setBounds(116, 107, 115, 20);
		CrearCuenta.add(textAPM);
		textAPM.setColumns(10);
		
		textNipCre = new JTextField();
		textNipCre.setBounds(116, 169, 115, 20);
		CrearCuenta.add(textNipCre);
		textNipCre.setColumns(10);
		
		
		JButton btnCrearCuenta = new JButton("Crear Cuenta");
		btnCrearCuenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				try{
					if(textNombre.getText().isEmpty() || textAPP.getText().isEmpty() || textAPM.getText().isEmpty() || textNoCuenta.getText().isEmpty() || textNipCre.getText().isEmpty() ) {
					error1.setVisible(true);
					JOptionPane.showMessageDialog(frame,
						    "Debe llenar todos los campos",
						    "Inane error",
						    JOptionPane.ERROR_MESSAGE);
					
					}else {
						Usuario p = new Usuario();
						String nombre =textNombre.getText().toString();
						String app = textAPP.getText().toString();
						String apm = textAPM.getText().toString();
						long nocuenta= Long.parseLong(textNoCuenta.getText());
						int nip = Integer.parseInt(textNipCre.getText());
						int idClient = Integer.parseInt(textidCliente.getText());
						p.setId(idClient);
						p.setNombre(nombre);
						p.setApellidoP(app);
						p.setApellidoM(apm);
						p.setCreditCard(nocuenta);
						p.setNip(nip);
						p.setFondos(200);
						c.insertarRegistro(p);
						
						ventana1.setVisible(false);
						
						int nipe=p.getNip();
						long nucena =p.getCreditCard();
						int ida=p.getId();
						
						Info ne= new Info();
						ne.setVisible(true);
						
						ne.enviarDato(apeidoM,apeidoP,nambre,fondos,ida,nipe,nucena);
						frame.setVisible(false);
						
						
						
					}
				}catch(Exception e) {
					
					//AQUI VA ERROR AL CREAR CUENTA
					JOptionPane.showMessageDialog(frame,
						    "OCURRIO UN PROBLEMA VUELVA A INTENTARLO",
						    "Inane error",
						    JOptionPane.ERROR_MESSAGE);
					textNombre.setText("");
					textAPP.setText("");
					textAPM.setText("");
					textNipCre.setText("");
					textNoCuenta.setText("");
					textidCliente.setText("");
					
				}
				
				
				}
		});
		btnCrearCuenta.setBounds(59, 227, 135, 20);
		CrearCuenta.add(btnCrearCuenta);
		
		JLabel lblApellidoMaterno = new JLabel("Apellido materno");
		lblApellidoMaterno.setBounds(10, 109, 106, 18);
		CrearCuenta.add(lblApellidoMaterno);
		
		JLabel lblNip_1 = new JLabel("NIP");
		lblNip_1.setBounds(10, 175, 46, 14);
		CrearCuenta.add(lblNip_1);
		
		JLabel lblNoCuenta = new JLabel("No. Cuenta");
		lblNoCuenta.setBounds(10, 138, 78, 20);
		CrearCuenta.add(lblNoCuenta);
		
		textNoCuenta = new JTextField();
		textNoCuenta.setBounds(116, 138, 115, 20);
		CrearCuenta.add(textNoCuenta);
		textNoCuenta.setColumns(10);
		
		JLabel idU = new JLabel("No. de cliente");
		idU.setBounds(10, 22, 106, 14);
		CrearCuenta.add(idU);
		
		textidCliente = new JTextField();
		textidCliente.setBounds(116, 19, 86, 20);
		CrearCuenta.add(textidCliente);
		textidCliente.setColumns(10);
		
		JLabel lblBienvenidoASu = new JLabel("BIENVENIDO A SU BANCO :D");
		lblBienvenidoASu.setBounds(120, 37, 385, 30);
		ventana1.add(lblBienvenidoASu);
		lblBienvenidoASu.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 24));
		
		JButton btnCrearCuentav1 = new JButton("Crear Cuenta");
		btnCrearCuentav1.setBounds(453, 89, 154, 30);
		ventana1.add(btnCrearCuentav1);
		btnCrearCuentav1.setFont(new Font("Verdana", Font.BOLD, 14));
		btnCrearCuentav1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//APARECERA EL JPANEL PARA CREAR CUENTA
				
				IniciarSesion.setVisible(false);
				CrearCuenta.setVisible(true);
			}
		});
		
		JButton btnIniciarSesion = new JButton("Iniciar Sesion");
		btnIniciarSesion.setBounds(109, 89, 162, 30);
		ventana1.add(btnIniciarSesion);
		btnIniciarSesion.setFont(new Font("Verdana", Font.BOLD, 14));
		btnIniciarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			
				//APARECERA EL JPANEL PARA INICIAR SESION
				
				CrearCuenta.setVisible(false);
				IniciarSesion.setVisible(true);
				
			}
		});
		
		
		textCuenta = new JTextField();
		textCuenta.setBounds(94, 26, 136, 23);
		IniciarSesion.add(textCuenta);
		textCuenta.setColumns(10);
		
		JLabel lblNumeroDeCuenta = new JLabel("No. de cuenta");
		lblNumeroDeCuenta.setBounds(10, 26, 88, 23);
		IniciarSesion.add(lblNumeroDeCuenta);
		
		JLabel lblNip = new JLabel("NIP");
		lblNip.setBounds(10, 82, 34, 20);
		IniciarSesion.add(lblNip);
		
		JLabel error = new JLabel("Debe llenar todos los campos");
		error.setFont(new Font("DialogInput", Font.BOLD | Font.ITALIC, 12));
		error.setForeground(Color.RED);
		error.setVisible(false);
		error.setBounds(31, 199, 213, 14);
		IniciarSesion.add(error);
		
		textNip = new JTextField();
		textNip.setBounds(94, 82, 136, 20);
		IniciarSesion.add(textNip);
		textNip.setColumns(10);
		
		JButton btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Usuario u = new Usuario();
					if(textCuenta.getText().isEmpty() || textNip.getText().isEmpty()) {
						error.setVisible(true);
					}else {
						p.setCreditCard(Long.parseLong(textCuenta.getText().toString()));
						p.setNip(Integer.parseInt(textNip.getText().toString()));
						u = c.seleccionarUsuario(u);
						if( (u.getCreditCard()==p.getCreditCard()) && (u.getNip()==p.getNip())) {
							ventana1.setVisible(false);
							
							int nipe=u.getNip();
							long nucena =u.getCreditCard();
							int ida=u.getId();
							apeidoM = u.getApellidoM();
							apeidoP = u.getApellidoP();
							nambre =u.getNombre();
							fondos = u.getFondos();
							
							
							Info ne= new Info();
							ne.setVisible(true);
							
							ne.enviarDato(apeidoM,apeidoP,nambre,fondos,ida,nipe,nucena);
							frame.setVisible(false);
							
						}else {
							errorSesion();
							textNip.setText("");
							textCuenta.setText("");
							
						}
						
						
					}
					
				
				}
				catch(Exception e) {
					//Aqui debo mandar mensaje de error
					System.out.println(e);
				}
			}
		});
		btnIngresar.setBounds(79, 224, 88, 23);
		IniciarSesion.add(btnIngresar);
		
		
		
		
		
		
	}
	public void errorSesion() {
		JOptionPane.showMessageDialog(frame,
			    "ERROR DE INICIO SE SESION INTENTE NUEVAMENTE.",
			    "Inane error",
			    JOptionPane.ERROR_MESSAGE);
		
	}

}
