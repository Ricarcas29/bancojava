

public class Usuario {
	int id;
	String nombre,ApellidoP,ApellidoM;
	int Nip;
	long creditCard;
	float fondos;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getNip() {
		return Nip;
	}
	public void setNip(int nip) {
		Nip = nip;
	}
	public long getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(long creditCard) {
		this.creditCard = creditCard;
	}
	public float getFondos() {
		return fondos;
	}
	public void setFondos(float fondos) {
		this.fondos = fondos;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getApellidoP() {
		return ApellidoP;
	}
	public void setApellidoP(String apellidoP) {
		ApellidoP = apellidoP;
	}
	public String getApellidoM() {
		return ApellidoM;
	}
	public void setApellidoM(String apellidoM) {
		ApellidoM = apellidoM;
	}
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nombre=" + nombre + ", ApellidoP=" + ApellidoP + ", ApellidoM=" + ApellidoM
				+ ", Nip=" + Nip + ", creditCard=" + creditCard + ", fondos=" + fondos + "]";
	}
	
	
	
}
