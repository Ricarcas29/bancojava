
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.util.ArrayList;
import java.util.List;



public class Conexion {

	private ObjectContainer db =null; 
	 //Inicia la BD
	private void abrirRegistro()
	{
		db = Db4oEmbedded.openFile("RegistroUsuario");
	}
	//CIERRA LA BD
	private void cerrarConexion()
	{
		db.close();
	}
	
	//PERMITE INSERTAR REGISTRO
	public void insertarRegistro(Usuario p)
	{
		abrirRegistro();
		db.store(p);
		cerrarConexion();
	}
	
	public Usuario seleccionarUsuario(Usuario p) {
		abrirRegistro();
		ObjectSet resultado=db.queryByExample(p);
		Usuario usuario=(Usuario) resultado.next();
		cerrarConexion();
		return usuario;
	}
	
	public void actualizarRegistro(int id,String nom,float fondI,long numTarjeta, int nIP) {

		abrirRegistro();
		Usuario p = new Usuario();
		p.setCreditCard(numTarjeta);
		ObjectSet resultado=db.queryByExample(p);
		Usuario preresultado=(Usuario) resultado.next();
		preresultado.setId(id);
		preresultado.setNombre(nom);
		preresultado.setNip(nIP);
		preresultado.setFondos(fondI);
		db.store(preresultado);
		cerrarConexion();
	}
	
	
	
	
}
