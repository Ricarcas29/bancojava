import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Info extends JFrame {

	private JPanel contentPane;
	protected Object forma;
	private JTextField textRetiro;
	private JTextField textDeposito;
	
	String nombre,app,apm;
	float fondos;
	int id, nip;
	Long Nocuenta;
	Usuario us = new Usuario();
	Conexion co= new Conexion();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Info frame = new Info();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Info() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 821, 530);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(0, 0, 805, 491);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(203, 134, 199, 235);
		panel_1.setVisible(false);
		panel_3.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblIngreseLaCantidad_1 = new JLabel("Ingrese la cantidad a depositar");
		lblIngreseLaCantidad_1.setFont(new Font("Verdana", Font.ITALIC, 11));
		lblIngreseLaCantidad_1.setBounds(0, 23, 199, 27);
		panel_1.add(lblIngreseLaCantidad_1);
		
		textDeposito = new JTextField();
		textDeposito.setBounds(10, 78, 157, 20);
		panel_1.add(textDeposito);
		textDeposito.setColumns(10);
		
		JButton btnDepositar = new JButton("DEPOSITAR");
		btnDepositar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				
					int contador=0;
					if(textDeposito.getText().isEmpty()) {
						System.out.println("ERROR");
					}else {
						int depa = Integer.parseInt(textDeposito.getText());
						us.setId(id);
						us=co.seleccionarUsuario(us);
						if(depa<10000 && depa>0) {
							float temporal = us.getFondos();
							float suma = temporal+depa;
							//us.setFondos(suma);
							co.actualizarRegistro(id, nombre, suma, Nocuenta, nip);
							textDeposito.setText("");
		
							if (JOptionPane.showConfirmDialog(null, "�Deseas realizar otro movimiento?", "WARNING",
							        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							    // yes option
								contador++;
								if (contador==2){
									System.exit(0);
								}
								
							} else {
							    // no option
								System.exit(0);
								
							}
						}else {
							
						}
						
					}
				//AQUI VA EL METODO ACTUALIZAR
				}catch(Exception e) {
					System.out.println(e);
				}
			}
		});
		btnDepositar.setBounds(63, 156, 89, 23);
		panel_1.add(btnDepositar);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(412, 134, 157, 235);
		panel_2.setVisible(false);
		panel_3.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblFondosDisponibles = new JLabel("Fondos disponibles");
		lblFondosDisponibles.setFont(new Font("Verdana", Font.ITALIC, 11));
		lblFondosDisponibles.setBounds(10, 26, 137, 22);
		panel_2.add(lblFondosDisponibles);
		
		JLabel fondoUser = new JLabel("0");
		fondoUser.setBounds(51, 90, 46, 14);
		panel_2.add(fondoUser);
		
		JButton btnImprimir = new JButton("IMPRIMIR");
		btnImprimir.setBounds(35, 157, 89, 23);
		panel_2.add(btnImprimir);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 133, 184, 235);
		panel.setVisible(false);
		panel_3.add(panel);
		panel.setLayout(null);
		
		textRetiro = new JTextField();
		textRetiro.setBounds(10, 80, 157, 20);
		panel.add(textRetiro);
		textRetiro.setColumns(10);
		
		JLabel lblIngreseLaCantidad = new JLabel("Ingrese la cantidad a retirar");
		lblIngreseLaCantidad.setFont(new Font("Verdana", Font.ITALIC, 11));
		lblIngreseLaCantidad.setBounds(10, 24, 167, 20);
		panel.add(lblIngreseLaCantidad);
		
		JButton btnRetirar = new JButton("RETIRAR");
		btnRetirar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//AQUI VA EL METODO ACTUALIZAR
				try {
					
					if(textRetiro.getText().isEmpty()) {
						System.out.println("ERROR DEBE INGRESAR UN DATO");
					}else {
						float reto = Float.parseFloat(textRetiro.getText());
						Usuario tota = new Usuario();
						tota.setId(id);
						tota=co.seleccionarUsuario(tota);
						float eui = tota.getFondos();
						if(reto>eui) {
							System.out.println("NO CUENTA CON LOS FONDOS SUFICIENTES PARA HACER EL RETIRO");
						}else {
							int contador=0;
							//AQUI VAN LAS ACCIONES PARA ACTUALIZAR LOS DATOS
							float tempo = tota.getFondos();
							System.out.println(tempo);
							float resta = tempo-reto;
							co.actualizarRegistro(id, nombre, resta, Nocuenta, nip);
							textRetiro.setText("");
							
							if (JOptionPane.showConfirmDialog(null, "�Deseas realizar otro movimiento?", "WARNING",
							        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							    // yes option
								contador++;
								if (contador==2){
									System.exit(0);
								}
								
							} else {
							    // no option
								System.exit(0);
								
							}
							
						}
						
					}
				}catch(Exception e) {
					System.out.println(e);
				}
				
			}
		});
		btnRetirar.setBounds(48, 159, 89, 23);
		panel.add(btnRetirar);
		
		
		JLabel lblBienvenido = new JLabel("BIENVENIDO");
		lblBienvenido.setBounds(351, 9, 132, 23);
		panel_3.add(lblBienvenido);
		lblBienvenido.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 18));
		
		JButton btnRetiro = new JButton("Retiro");
		btnRetiro.setBounds(10, 67, 157, 31);
		panel_3.add(btnRetiro);
		btnRetiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				panel.setVisible(true);
				panel_1.setVisible(false);
				panel_2.setVisible(false);
			}
		});
		btnRetiro.setFont(new Font("Verdana", Font.ITALIC, 18));
		
		JButton btnDeposito = new JButton("Deposito");
		btnDeposito.setBounds(218, 67, 157, 31);
		panel_3.add(btnDeposito);
		btnDeposito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panel.setVisible(false);
				panel_1.setVisible(true);
				panel_2.setVisible(false);
				
			}
		});
		btnDeposito.setFont(new Font("Verdana", Font.ITALIC, 18));
		
		JButton btnFondos = new JButton("Fondos");
		btnFondos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Usuario uta=new Usuario();
				uta.setId(id);
				uta = co.seleccionarUsuario(uta);
				panel.setVisible(false);
				panel_1.setVisible(false);
				panel_2.setVisible(true);
				
				String fanda = String.valueOf(uta.getFondos());
				fondoUser.setText(fanda);
			}
		});
		btnFondos.setBounds(412, 67, 157, 31);
		panel_3.add(btnFondos);
		btnFondos.setFont(new Font("Verdana", Font.ITALIC, 18));
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(629, 67, 104, 31);
		panel_3.add(btnSalir);
		btnSalir.setFont(new Font("Verdana", Font.ITALIC, 18));
		
	}
	
	public void enviarDato(String app, String apm, String nombre, float fondos, int id,int nip, long cuenta)  {
	
		this.nombre=nombre;
		this.app=app;
		this.apm=apm;
		this.fondos=fondos;
		this.id=id;
		this.nip=nip;
		this.Nocuenta=cuenta;
		
	}
	
}
